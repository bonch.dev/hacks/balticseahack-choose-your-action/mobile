import 'package:flutter/material.dart';
import 'common_styles.dart';

class UserCoursesWidget extends StatefulWidget {
  @override
  _UserCoursesWidgetState createState() => _UserCoursesWidgetState();
}

class _UserCoursesWidgetState extends State<UserCoursesWidget> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Color(0xFFFAFAFA),
          floating: false,
          bottom: PreferredSize(
            // Add this code
            preferredSize: Size.fromHeight(50.0), // Add this code
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.fromLTRB(20.0, 20.0, 0.0, 0.0),
                      child: Text("Мои секции",
                          style: getBlackTextStyle(fontSize: 26.0))),
                  Container(
                      padding: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 20.0),
                      child: Container())
                ],
              ),
              color: Color(0xFFFAFAFA),
            ), // Add this code
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate((context, position) {
            return GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/courseScheduleWidget");
                },
                child: Container(
                    margin:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 20.0),
                          height: 65.0,
                          child: Image.asset("assets/course_img.png"),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.symmetric(vertical: 5.0),
                              child: Text("Брасс боком", style: getBlackSmallTextStyle(),),
                            ),
                            Container(
                              child: Text("Какой-то текст", style: getGreyTextStyle(),),
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 10.0),
                              height: 1.0,
                              width: 180.0,
                              color: Color(0x55666666),
                            ),
                          ],

                        ),

                      ],
                    )));
          }, childCount: 3),
        ),
      ],
    );
  }
}

//return Container(
//margin: EdgeInsets.symmetric(horizontal: 30.0),
//child: Row(
//children: <Widget>[Image.asset("assets/course_img.png")],
//));
