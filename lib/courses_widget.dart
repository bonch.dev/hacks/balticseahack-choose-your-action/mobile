import 'package:flutter/material.dart';
import 'common_styles.dart';

class CoursesWidget extends StatefulWidget {
  @override
  _CoursesWidgetState createState() => _CoursesWidgetState();
}

class _CoursesWidgetState extends State<CoursesWidget> {

  List<String> _filterNames = List.from([
    "Фильтры - 2",
    "Очень",
    "Классные",
    "Фильтры",
    "Прям",
    "Обожаю",
    "Блин"
  ]);

  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Color(0xFFFAFAFA),
          floating: false,
          bottom: PreferredSize(
            // Add this code
            preferredSize: Size.fromHeight(200.0), // Add this code
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.fromLTRB(20.0, 44.0, 0.0, 0.0),
                      child:
                      Text("Санкт-Петербург", style: getGreyTextStyle())),
                  Container(
                      margin: EdgeInsets.fromLTRB(20.0, 20.0, 0.0, 0.0),
                      child: Text("Лента", style: getBlackTextStyle())),
                  Container(
                    margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                    decoration: new BoxDecoration(
                        color: Color(0xFFFFFFFF),
                        borderRadius: new BorderRadius.circular(2.0)),
                    child: TextField(
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.search),
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 10.0),
                          focusedBorder: InputBorder.none,
                          border: InputBorder.none,
                          hintText: 'Попробуйте "Водное поло"'),
                    ),
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(0.0, 15.0, 0.0, 15.0),
                      height: 35.0,
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: _filterNames.length,
                          itemBuilder: (context, position) {
                            return Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Colors.black, width: 0.5),
                                  borderRadius: BorderRadius.circular(3.0),
                                  color: Colors.white,
//                                boxShadow: <BoxShadow>[
//                                  BoxShadow(
//                                    color: Color(0xFFfcfcfc),
//                                    offset: Offset(1.0, 1.0),
//                                    blurRadius: 1.0,
//                                  ),
//                                ],
                                ),
                                margin: EdgeInsets.fromLTRB(
                                    position == 0 ? 20.0 : 5.0,
                                    0.0,
                                    5.0,
                                    0.0),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5.0, vertical: 10.0),
                                child: Text(_filterNames[position]));
                          })),
                ],
              ),
              color: Color(0xFFFAFAFA),
            ), // Add this code
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate((context, position) {
            return GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/courseWidget");
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 10.0),
                  decoration: BoxDecoration(
                      color: Color(0xFFFAFAFA),
                      border: Border(),
                      borderRadius: BorderRadius.circular(3.0)),
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Image.asset("assets/swimmer.png"),
                          Align(
                            child: Container(
                                margin: EdgeInsets.fromLTRB(0.0, 10.0, 10.0, 0.0),
                                child: Image.asset("assets/fav_course.png"),
                                width: 30.0
                            ),
                            alignment: Alignment.topRight,
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text(
                                "Kangaroo Valley Safari",
                                style: getBlackTextStyle(),
                              ),
                              margin: EdgeInsets.only(bottom: 5.0),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 5.0),
                              width:
                              MediaQuery.of(context).size.width - 20.0,
                              child: Text(
                                  "Открывается набор в секцию по водному полу детей от 12 до 16 лет!!!"),
                            ),
                            Container(
                              child: Text("Vodolaz group",
                                  style: getGreyTextStyle(fontSize: 14.0)),
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text("28 окт."))
                          ],
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                      ),
                    ],
                  ),
                ));
          }, childCount: 19),
        ),
      ],
    );
  }
}