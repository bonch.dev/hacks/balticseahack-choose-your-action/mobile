import 'package:baltic_sea_hack/chats_widget.dart';
import 'package:baltic_sea_hack/favorite_widget.dart';
import 'package:flutter/material.dart';
import 'common_styles.dart';
import 'courses_widget.dart';
import 'user_courses_widget.dart';
import 'course_schedule_dart.dart';
import 'common_styles.dart';

class ProfileWidget extends StatefulWidget {
  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Colors.white,
          floating: false,
          bottom: PreferredSize(
            // Add this code
            preferredSize: Size.fromHeight(50.0), // Add this code
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.fromLTRB(20.0, 20.0, 0.0, 0.0),
                      child: Text("Мой профиль",
                          style: getBlackTextStyle(fontSize: 26.0))),
                  Container(
                      padding: EdgeInsets.fromLTRB(0.0, 00.0, 0.0, 20.0),
                      child: Container())
                ],
              ),
            ), // Add this code
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
            [
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Stack(
                    children: <Widget>[
                      Container(child: Image.asset("assets/jora.png")),
                      Positioned(
                        child: Container(
                          child: Icon(
                            Icons.edit,
                            color: Colors.white,
                          ),
                          width: 30.0,
                        ),
                        top: 10.0,
                        right: 10.0,
                      )
                    ],
                  )),
              Container(
                margin: EdgeInsets.fromLTRB(15.0, 10.0, 0.0, 20.0),
                child: Text(
                  "Жора Крапива, 19 лет",
                  style: getBlackTextStyle(
                      fontSize: 30.0, weight: FontWeight.w400),
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 20.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 20.0,
                      child: Image.asset("assets/phone.png"),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Text("+7 (965) 845-22-36",
                          style:
                              TextStyle(color: Colors.black, fontSize: 18.0)),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
