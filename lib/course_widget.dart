import 'package:baltic_sea_hack/common_styles.dart';
import 'package:flutter/material.dart';

class CourseWidget extends StatefulWidget {
  @override
  _CourseWidgetState createState() => _CourseWidgetState();
}

class _CourseWidgetState extends State<CourseWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  color: Colors.greenAccent,
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width,
                ),
                Align(
                    alignment: Alignment.center,
                    child: Container(
                      child: Image.asset("assets/img_slider.png"),
                      margin: EdgeInsets.symmetric(vertical: 10.0),
                    )),
                Container(
                    child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 15.0),
                  child: Text(
                    "Kangaroo Valley Safari",
                    style: getBlackTextStyle(),
                  ),
                )),
                Container(
                    child: Container(
                  margin: EdgeInsets.fromLTRB(15.0, 15.0, 15.0, 0.0),
                  child: Text(
                    "Сайт",
                    style: getBlackSmallTextStyle(),
                  ),
                )),
                Container(
                    child: Container(
                  margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                  child: Text(
                    "Сайт",
                    style: getBlackSmallTextStyle(),
                  ),
                )),
                Container(
                    child: Container(
                  margin: EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 0.0),
                  child: Text(
                    "Сайт",
                    style: getBlackSmallTextStyle(),
                  ),
                )),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 0.0),
                  child: Text(
                    "text, text, text, text, text, text, text, text, text, text, text, "
                    "text, text, text, text, text, text, "
                    "text, text, text, text, text, text, "
                    "text, text, text, text, text, text, "
                    "text, text, text, text, text, text, "
                    "text, text",
                    style: getGreyTextStyle(),
                  ),
                ),
                Container(
                    child: Container(
                  margin:
                      EdgeInsets.symmetric(horizontal: 25.0, vertical: 15.0),
                  child: Text(
                    "Отзывы",
                    style: getBlackTextStyle(fontSize: 21.0),
                  ),
                )),
                Container(
                    margin: EdgeInsets.symmetric(horizontal: 25.0),
                    child: Row(
                      children: <Widget>[
                        Text("Евгений Щука",
                            style: getBlackTextStyle(fontSize: 18.0))
                      ],
                    )),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 0.0),
                  child: Text(
                    "Как вы думаете, почему я щука ?Потому что я кусал своих ВРАГОВ. Меня научили именно в этой секции кусаться. Я вам рекомендую. ",
                    style: getGreyTextStyle(),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.fromLTRB(25.0, 10.0, 25.0, 15.0),
                  child: Text(
                    "Посмотреть все отзывы (21)",
                    style: getBlueTextStyle(),
                  ),
                ),
                FlatButton(
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            title: Text("Спасибо за заявку"),
                            content: Text(
                                "Администратор свяжется с вами в ближайшее время",
                                style: getGreyTextStyle()),
                            actions: <Widget>[
                              new FlatButton(
                                child: new Text("ОК"),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          color: Color(0xFF007AFF),
                          borderRadius: BorderRadius.circular(5.0)),
                      height: 60.0,
                      margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                      child: Align(
                        alignment: Alignment.center,
                        child: Text(
                          "Оставить заявку",
                          style: getWhiteThickTextStyle(),
                        ),
                      ),
                    )),
              ],
            ),
          )),
    );
  }
}

//Spacer(),
//Container(
//width: MediaQuery.of(context).size.width,
//decoration: BoxDecoration(
//color: Color(0xFF007AFF),
//borderRadius: BorderRadius.circular(5.0)),
//height: 60.0,
//margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 0.0),
//child: Align(
//alignment: Alignment.center,
//child: Text(
//"Оставить заявку",
//style: getWhiteThickTextStyle(),
//),
//),
//),
