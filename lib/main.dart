import 'package:baltic_sea_hack/course_schedule_dart.dart';
import 'package:baltic_sea_hack/chat_widget.dart';
import 'package:flutter/material.dart';
import 'main_widget.dart';
import 'course_widget.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{
        "/courseWidget": (BuildContext context) => CourseWidget(),
        "/chatWidget": (BuildContext context) => ChatWidget(),
        "/courseScheduleWidget": (BuildContext context) =>
            CourseScheduleWidget(),
      },
      theme: ThemeData(
//        primarySwatch: Colors.grey
        primaryColor: MaterialColor(
          0xFF019AE8,
          <int, Color>{
            50: Color(0xFF019AE8),
            100: Color(0xFF019AE8),
            200: Color(0xFF019AE8),
            300: Color(0xFF019AE8),
            350: Color(0xFF019AE8),
            // only for raised button while pressed in light theme
            400: Color(0xFF019AE8),
            500: Color(0xFF019AE8),
            600: Color(0xFF019AE8),
            700: Color(0xFF019AE8),
            800: Color(0xFF019AE8),
            850: Color(0xFF019AE8),
            // only for background color in dark theme
            900: Color(0xFF019AE8),
          },
        ),
      ),
      home: MainWidget(),
    );
  }
}
