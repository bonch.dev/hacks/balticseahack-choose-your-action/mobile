import 'package:flutter/material.dart';
import 'common_styles.dart';

class ChatWidget extends StatefulWidget {
  @override
  _ChatWidgetState createState() => _ChatWidgetState();
}

class _ChatWidgetState extends State<ChatWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          backgroundColor: Color(0xFFF9F9F9),
          elevation: 0.0,
          leading: new IconButton(
            icon: new Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Брасс боком", style: getBlackTextStyle())),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              getTutorMessage(
                  "Здравствуйте, вы оставили заявку на секцию брасс боком. Ну что, когда собираетесь навалить с нами боком?"),
              getTimeStamp(),
              getMyMessage("Даа,я очень хочу наваливать как вы и мои друзья. Всю жизнь мечтал брассиком плавать в озере!"),
              getTimeStamp(),
              getTutorMessage(
                  "Здравствуйте, вы оставили заявку на секцию брасс боком. Ну что, когда собираетесь навалить с нами боком "),
              getTimeStamp(),
              getTutorMessage(
                  "Здравствуйте, вы оставили заявку на секцию брасс боком. Ну что, когда собираетесь навалить с нами боком?"),
              getTimeStamp(),
              getMyMessage("Даа,я очень хочу наваливать как вы и мои друзья. Всю жизнь мечтал брассиком плавать в озере!"),
              getTimeStamp(),
              getTutorMessage(
                  "Здравствуйте, вы оставили заявку на секцию брасс боком. Ну что, когда собираетесь навалить с нами боком "),
              Container(height: 70.0,)

            ],
          ),
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,

                      border: Border(
                          top: BorderSide(

                              width: 1.0, color: Color(0xFFE6E6E6)))),
                  child: TextField(
                    keyboardType: TextInputType.multiline,
                    maxLines: null,
                    textInputAction: TextInputAction.newline,
                    decoration: InputDecoration(
                      suffixIcon:
                          Container(width: 30.0, child: Icon(Icons.send)),
                      prefixIcon: Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          width: 30.0,
                          child: Icon(Icons.add)),
                      contentPadding: EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 10.0),
                      focusedBorder: InputBorder.none,
                      border: InputBorder.none,
                      hintText: 'Введите текст сообщения',
                    ),
                  ))),
        ],
      ),
    );
  }

  Widget getTutorMessage(String text) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        CircleAvatar(
          radius: 20.0,
          backgroundImage: AssetImage("assets/swimmer.png"),
          backgroundColor: Colors.transparent,
        ),
        Container(
          margin: EdgeInsets.fromLTRB(10.0, 10.0, 30.0, 0.0),
          padding: EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width - 80.0,
          child: Text(
            text,
            style: getWhiteThinTextStyle(),
          ),
          decoration: BoxDecoration(
              color: Color(0xFF007AFF),
              borderRadius: BorderRadius.circular(5.0)),
        )
      ],
    );
  }


  Widget getMyMessage(String text) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(30.0, 10.0, 10.0, 0.0),
          padding: EdgeInsets.all(10.0),
          width: MediaQuery.of(context).size.width - 80.0,
          child: Text(
            text,
            style: getBlackSmallTextStyle(fontSize: 17.5),
          ),
          decoration: BoxDecoration(
              border: Border.all(color: Color(0xFFC8C7CC)),
              borderRadius: BorderRadius.circular(5.0)),
        ),
        CircleAvatar(
          radius: 20.0,
          backgroundImage: AssetImage("assets/swimmer.png"),
          backgroundColor: Colors.transparent,
        ),
      ],
    );
  }

  Widget getTimeStamp() {
    return Container(
      margin: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 7.0),
      child: Center(child: Text("Friday・15:23", style: getGreyTextStyle())),
    );
  }
}
