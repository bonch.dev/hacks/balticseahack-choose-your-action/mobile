import 'package:flutter/material.dart';
import 'common_styles.dart';

class ChatsWidget extends StatefulWidget {
  @override
  _ChatsWidgetState createState() => _ChatsWidgetState();
}

class _ChatsWidgetState extends State<ChatsWidget> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Color(0xFFFAFAFA),
          floating: false,
          bottom: PreferredSize(
            // Add this code
            preferredSize: Size.fromHeight(180.0), // Add this code
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.fromLTRB(20.0, 20.0, 0.0, 0.0),
                      child: Text("Сообщения",
                          style: getBlackTextStyle(fontSize: 26.0))),
                  Container(
                      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 20.0),
                      child: Container(
                          height: 110.0,
                          child: ListView.builder(
                              itemBuilder: (context, position) {
                                return position == 1
                                    ? Container(
                                        margin: EdgeInsets.only(right: 15.0),
                                        height: 50.0,
                                        width: 3.0,
                                        color: Color(0xFFC4C4C4),
                                      )
                                    : Container(
                                        padding: EdgeInsets.fromLTRB(
                                            position == 0 ? 40.0 : 0.0,
                                            10.0,
                                            position == 0 ? 15.0 : 25.0,
                                            10.0),
                                        child: Column(
                                          children: <Widget>[
                                            Container(
                                                width: 70.0,
                                                height: 70.0,
                                                //image,
                                                color: Colors.deepPurpleAccent),
                                            Text("Все")
                                          ],
                                        ));
                              },
                              scrollDirection: Axis.horizontal,
                              itemCount: 19)))
                ],
              ),
              color: Color(0xFFFAFAFA),
            ), // Add this code
          ),
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate((context, position) {
            return GestureDetector(
                onTap: () {
                  Navigator.of(context).pushNamed("/chatWidget");
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  child: Row(
                    children: <Widget>[
                      Container(
                        child: CircleAvatar(
                          radius: 30.0,
                          backgroundImage: AssetImage("assets/swimmer.png"),
                          backgroundColor: Colors.transparent,
                        ),
                        margin: EdgeInsets.only(right: 20.0),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              child: Text(
                            "Имя и фамилия (Тренер)",
                            style: getBlackSmallTextStyle(),
                          ),
                          margin: EdgeInsets.only(bottom: 8.0),),
                          Container(
                              child: Text(
                                "Текст сообщения",
                                style: getGreyTextStyle(),
                              ))
                        ],
                      )
                    ],
                  ),
                ));
          }, childCount: 19),
        ),
      ],
    );
  }
}
