import 'package:flutter/material.dart';
import 'common_styles.dart';

class FavouriteWidget extends StatefulWidget {
  @override
  _FavouriteWidgetState createState() => _FavouriteWidgetState();
}

class _FavouriteWidgetState extends State<FavouriteWidget> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          floating: false,
          bottom: PreferredSize(
            // Add this code
            preferredSize: Size.fromHeight(50.0), // Add this code
            child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                    margin: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 10.0),
                    child: Text("Избранное",
                        style: getBlackTextStyle()))), // Add this code
          ),
          backgroundColor: Colors.white,
        ),
        SliverList(
          delegate: SliverChildBuilderDelegate((context, position) {
            return GestureDetector(
                onTap: () {
                  Navigator.pushNamed(context, "/courseWidget");
                },
                child: Container(
                  margin: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 10.0),
                  decoration: BoxDecoration(
                      color: Color(0xFFFAFAFA),
                      border: Border(),
                      borderRadius: BorderRadius.circular(3.0)),
                  child: Column(
                    children: <Widget>[
                      Stack(
                        children: <Widget>[
                          Image.asset("assets/swimmer.png"),
                          Align(
                            child: Container(
                              margin: EdgeInsets.fromLTRB(0.0, 10.0, 10.0, 0.0),
                              child: Image.asset("assets/fav_course.png"),
                              width: 30.0
                            ),
                            alignment: Alignment.topRight,
                          ),
                        ],
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 0.0),
                        child: Column(
                          children: <Widget>[
                            Container(
                              child: Text(
                                "Kangaroo Valley Safari",
                                style: getBlackTextStyle(),
                              ),
                              margin: EdgeInsets.only(bottom: 5.0),
                            ),
                            Container(
                              margin: EdgeInsets.only(bottom: 5.0),
                              width: MediaQuery.of(context).size.width - 20.0,
                              child: Text(
                                  "Открывается набор в секцию по водному полу детей от 12 до 16 лет!!!"),
                            ),
                            Container(
                              child: Text("Vodolaz group",
                                  style: getGreyTextStyle(fontSize: 14.0)),
                            ),
                            Align(
                                alignment: Alignment.centerRight,
                                child: Text("28 окт."))
                          ],
                          crossAxisAlignment: CrossAxisAlignment.start,
                        ),
                      ),
                    ],
                  ),
                ));
          }, childCount: 19),
        ),
      ],
    );
  }
}
