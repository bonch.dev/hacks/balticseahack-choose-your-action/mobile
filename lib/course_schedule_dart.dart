import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'common_styles.dart';

class CourseScheduleWidget extends StatefulWidget {
  @override
  _CourseScheduleWidgetState createState() => _CourseScheduleWidgetState();
}

class _CourseScheduleWidgetState extends State<CourseScheduleWidget> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
            backgroundColor: Color(0xFFF9F9F9),
            elevation: 0.0,
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            ),
            title: Text("Брасс боком", style: getBlackTextStyle())),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(vertical: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "13 окт. - 20 окт.",
                      style: getGreyTextStyle(fontSize: 18.0),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 10.0),
                        width: 25.0,
                        child: GestureDetector(
                          onTap: () {
                            _selectDate(context);
                          },
                          child: Image.asset("assets/calendar.png"),
                        ))
                  ],
                ),
              ),
              Container(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                      child: Column(
                    children: <Widget>[
                      Text("13", style: getBlueTextStyle(fontSize: 35.0)),
                      Text("окт.", style: getBlueTextStyle(fontSize: 20.0)),
                    ],
                  )),
                  Container(
                    margin: EdgeInsets.only(left: 10.0),
                    child: Column(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 8.0),
                          height: 1.0,
                          width: 250.0,
                          color: Color(0xFF4285F4),
                        ),
                        Container(
                          padding: EdgeInsets.all(10.0),
                          width: 250.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Занятия на суше",
                                style: getWhiteThinTextStyle(),
                              ),
                              Text(
                                "11:30-12:30",
                                style: getWhiteThinTextStyle(),
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFF039BE5),
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 10.0),
                          padding: EdgeInsets.all(10.0),
                          width: 250.0,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                "Занятия на суше",
                                style: getWhiteThinTextStyle(),
                              ),
                              Text(
                                "11:30-12:30",
                                style: getWhiteThinTextStyle(),
                              ),
                            ],
                          ),
                          decoration: BoxDecoration(
                              color: Color(0xFF039BE5),
                              borderRadius: BorderRadius.circular(5.0)),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
              Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          child: Column(
                        children: <Widget>[
                          Text("14",
                              style: getBlackSmallTextStyle(fontSize: 35.0)),
                          Text("окт.",
                              style: getBlackSmallTextStyle(fontSize: 20.0)),
                        ],
                      )),
                      Container(
                        margin: EdgeInsets.only(left: 10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(10.0),
                              width: 250.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Занятия на суше",
                                    style: getWhiteThinTextStyle(),
                                  ),
                                  Text(
                                    "11:30-12:30",
                                    style: getWhiteThinTextStyle(),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  color: Color(0xFF8A8A8F),
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          child: Column(
                        children: <Widget>[
                          Text("15",
                              style: getBlackSmallTextStyle(fontSize: 35.0)),
                          Text("окт.",
                              style: getBlackSmallTextStyle(fontSize: 20.0)),
                        ],
                      )),
                      Container(
                          width: 250.0,
                          child: Center(
                              child: Text("Занятий нет.",
                                  style: getBlackThickStyle()))),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          child: Column(
                        children: <Widget>[
                          Text("16",
                              style: getBlackSmallTextStyle(fontSize: 35.0)),
                          Text("окт.",
                              style: getBlackSmallTextStyle(fontSize: 20.0)),
                        ],
                      )),
                      Container(
                          width: 250.0,
                          child: Center(
                              child: Text("Занятий нет.",
                                  style: getBlackThickStyle()))),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                          child: Column(
                        children: <Widget>[
                          Text("15",
                              style: getBlackSmallTextStyle(fontSize: 35.0)),
                          Text("окт.",
                              style: getBlackSmallTextStyle(fontSize: 20.0)),
                        ],
                      )),
                      Container(
                        margin: EdgeInsets.only(left: 10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(10.0),
                              width: 250.0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "Занятия на суше",
                                    style: getWhiteThinTextStyle(),
                                  ),
                                  Text(
                                    "11:30-12:30",
                                    style: getWhiteThinTextStyle(),
                                  ),
                                ],
                              ),
                              decoration: BoxDecoration(
                                  color: Color(0xFF8A8A8F),
                                  borderRadius: BorderRadius.circular(5.0)),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          child: Column(
                        children: <Widget>[
                          Text("17",
                              style: getBlackSmallTextStyle(fontSize: 35.0)),
                          Text("окт.",
                              style: getBlackSmallTextStyle(fontSize: 20.0)),
                        ],
                      )),
                      Container(
                          width: 250.0,
                          child: Center(
                              child: Text("Занятий нет.",
                                  style: getBlackThickStyle()))),
                    ],
                  )),
              Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                          child: Column(
                        children: <Widget>[
                          Text("18",
                              style: getBlackSmallTextStyle(fontSize: 35.0)),
                          Text("окт.",
                              style: getBlackSmallTextStyle(fontSize: 20.0)),
                        ],
                      )),
                      Container(
                          width: 250.0,
                          child: Center(
                              child: Text("Занятий нет.",
                                  style: getBlackThickStyle()))),
                    ],
                  )),
              Container(height: 20.0)
            ],
          ),
        ));
  }

  Future<Null> _selectDate(BuildContext context) async {
    var selectedDate = DateTime(2019, 10, 13);
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
}
