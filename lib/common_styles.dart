import 'package:flutter/material.dart';

TextStyle getGreyTextStyle({fontSize = 16.0}) {
  return TextStyle(color: Color(0xFF666666), fontSize: fontSize);
}

TextStyle getBlackTextStyle({fontSize = 24.0, weight = FontWeight.w600}) {
  return TextStyle(color: Color(0xFF000000), fontSize: fontSize, fontWeight: weight);
}

TextStyle getBlackSmallTextStyle({fontSize: 18.0}) {
  return TextStyle(color: Color(0xFF000000), fontSize: fontSize, fontWeight: FontWeight.w400);
}

TextStyle getBlackThickStyle() {
  return TextStyle(color: Color(0xFF000000), fontSize: 18.0, fontWeight: FontWeight.w500);
}

TextStyle getWhiteThickTextStyle() {
  return TextStyle(color: Color(0xFFFFFFFF), fontSize: 20.0, fontWeight: FontWeight.w500);
}

TextStyle getWhiteThinTextStyle() {
  return TextStyle(color: Color(0xFFFFFFFF), fontSize: 17.5, fontWeight: FontWeight.w400);
}

TextStyle getBlueTextStyle({fontSize: 16.0}) {
  return TextStyle(color: Color(0xFF007AFF), fontSize: fontSize);
}

