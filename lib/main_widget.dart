import 'package:baltic_sea_hack/chats_widget.dart';
import 'package:baltic_sea_hack/favorite_widget.dart';
import 'package:flutter/material.dart';
import 'courses_widget.dart';
import 'user_courses_widget.dart';
import 'course_schedule_dart.dart';
import 'profile_widget.dart';

class MainWidget extends StatefulWidget {
  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> {
  int currentSelection = 4;

  @override
  Widget build(BuildContext context) {
    var widget;
    switch (currentSelection) {
      case 0:
        widget = CoursesWidget();
        break;
      case 1:
        widget = FavouriteWidget();
        break;
      case 2:
        widget = ChatsWidget();
        break;
      case 3:
        widget = UserCoursesWidget();
        break;
      case 4:
        widget = ProfileWidget();
        break;
    }

    return Scaffold(
        backgroundColor: Color(0xFFFFFFFF),
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/search.png"),
                    color: Colors.black),
                title: Text("1")),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/fav.png"),
                    color: Colors.black),
                title: Text("2")),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/list.png"),
                    color: Colors.black),
                title: Text("3")),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/message.png"),
                    color: Colors.black),
                title: Text("4")),
            BottomNavigationBarItem(
                icon: ImageIcon(AssetImage("assets/profile.png"),
                    color: Colors.black),
                title: Text("5")),
          ],
        ),
        body: widget
        );
  }
}
